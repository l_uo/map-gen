(ns luo.map-gen
  (:require [clojure.set :as s])
  (:gen-class))


(def ^:dynamic *directions* {:left [-1 0] :right [1 0] :up [0 -1] :down [0 1]})
(def ^:dynamic *rules*)
(def ^:dynamic *size*)
(def ^:dynamic *weights*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Entropy functions to decide which coordinate to collapse to a certain one state
; The position with the highest entropy will be chosen for collapse, to get the most
; variance in the generated map.
; Shannon entropy makes independent possibility (* chance-a chance-b ...)
; into sum form with the use of logarithmic identity: log(a*b) = log(a)+log(b)
; since logarithm on n < 1 produces a negative number, we get a positive entropy
; with -log(sum of all n).
; Resulting entropy number represents the average number of question+answers needed 
; to determine a randomly chosen value from a set of possibilities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn log2 [n]
  (/ (java.lang.Math/log n)
     (java.lang.Math/log 2)))

(defn shannon-entropy [possible]
  (if (>= 1 (count possible))
    0
    (let [[sum-weights sum-log-weights]
          (reduce (fn [[sw slw] tile]
                    (let [weight (tile *weights*)]
                      [(+ sw weight)
                       (+ slw
                          (* weight (log2 weight)))]))
                  [0 0]
                  possible)]
      (- (log2 sum-weights)
         (/ sum-log-weights sum-weights)))))

(defn shannon-entropy-all [matrix]
  (reduce (fn [mat xy]
            (assoc-in mat [xy :weight] (shannon-entropy
                                        (get-in mat [xy :possibilities]))))
          matrix
          (keys matrix)))

(defn get-highest-entropy-coordinate [matrix]
  (reduce (fn [highest xy-with-map]
            (let [highest-weight (:weight (second highest))
                  weight (:weight (second xy-with-map))]
              (cond (> weight highest-weight)
                    xy-with-map
                    (< weight highest-weight)
                    highest
                    :else (rand-nth [highest xy-with-map]))))
          (first matrix)
          matrix))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Functions for propagating the collapse effect to all tiles.
; This requires comparing a tiles state and the *rules* var, to remove
; tiles states that are no longer possible.
; Propagation effect is done until we see that a change in a tiles possibility
; state has no effect on its neighbours.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn valid-direction? [size-x size-y dir xy]
  (let [dir-xy (get *directions* dir)
        [x y] (map + xy dir-xy)]
    (if (and (<= 0 x (dec size-x))
             (<= 0 y (dec size-y)))
      true
      false)))

(defn any-neighbour-possible? [cur-pos-tiles neigh-pos-tile dir]
  (loop [[cur & curs] cur-pos-tiles
         res #{}]
    (cond
      (seq res) true
      (not cur) false
      :else (recur curs (if (get *rules* [cur neigh-pos-tile dir])
                          (conj res [cur neigh-pos-tile dir])
                          res)))))


(defn propagate
  ([xy coords] (propagate xy coords (conj [] xy)))

  ([_ coords stack]
   (if-not (peek stack)
     coords
     (let [xy (peek stack)
           stack (pop stack)
           possible-tiles (:possibilities (get coords xy))
           valid-dirs (filter #(valid-direction? (:x *size*) (:y *size*)
                                                 % xy)
                              [:up :down :left :right])
           removable-tiles (set
                            (for [dir valid-dirs
                                  :let [neighbour-xy (mapv + xy (get *directions* dir))
                                        not-possible-for-neighbour
                                        (into #{} (filter
                                                   #(not (any-neighbour-possible?
                                                          possible-tiles % dir))
                                                   (:possibilities (get coords neighbour-xy))))]]
                              [neighbour-xy not-possible-for-neighbour]))
           new-coords (reduce (fn [coords [neighbour-xy rem-tiles]]
                                (update-in coords [neighbour-xy :possibilities]
                                           #(s/difference % rem-tiles)))
                              coords
                              removable-tiles)
           new-stack (reduce (fn [stack [neighbour-xy rem-tiles]]
                               (if (seq rem-tiles)
                                 (conj stack neighbour-xy)
                                 stack))
                             stack
                             removable-tiles)]
       (recur _
              new-coords
              new-stack)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; functions for collapsing tiles state. Each collapse results in 
; one possible state for that specific tile. *weights* var is used to keep the
; frequency of the types of tiles comparable to the example map used.
; Collapsing is done until all tiles have only 1 possible state left.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn all-collapsed? [coords]
  (every? (fn [[_ v]] (= 1 (count (:possibilities v)))) coords))

(defn choose-possible-tile
  [[curr & rest] random]
  (let [random (- random (get *weights* curr))]
    (if (or (> 0 random) (not rest))
      curr
      (recur rest random))))

(defn collapse [xy coords]
  (let [possible (:possibilities (get coords xy))
        total-weights (reduce (fn [total pos]
                                (+ total (get *weights* pos)))
                              0
                              possible)
        chosen (choose-possible-tile possible (* (rand) total-weights))]
    (assoc-in coords [xy :possibilities] #{chosen})))

(defn collapse-all [map-of-coordinates]
  (if (all-collapsed? map-of-coordinates)
    map-of-coordinates
    (let [map-of-coordinates-with-weights (shannon-entropy-all map-of-coordinates)
          highest-entropy-xy (get-highest-entropy-coordinate map-of-coordinates-with-weights)
          map-of-coordinates-highest-collapsed (collapse (first highest-entropy-xy)
                                                         map-of-coordinates-with-weights)]
      (recur (propagate (first highest-entropy-xy)
                        map-of-coordinates-highest-collapsed)))))

; functions for setting dynamic vars & creating final result

(defn make-matrix-map [matrix]
  (let [all-possibilities (set (apply concat matrix))]
    (apply merge (for [x (range (:x *size*))
                       y (range (:y *size*))]
                   {[x y] {:possibilities all-possibilities}}))))

(defn make-rules [matrix]
  (let [col-count (count matrix)
        row-count (count (first matrix))]
    (into #{}
          (apply concat (for [y (range col-count)
                              x (range row-count)
                              :let [dirs (filter #(valid-direction? row-count col-count % [x y])
                                                 [:up :down :left :right])]]
                          (map (fn [dir]
                                 (let [[next-x next-y] (get *directions* dir)]
                                   [(get-in matrix [y x])
                                    (get-in matrix [(+ y next-y) (+ x next-x)])
                                    dir]))
                               dirs))))))

(defn parse-weights [matrix]
  (frequencies (flatten matrix)))

(defn parse-size [matrix]
  {:x (count (first matrix))
   :y (count matrix)})

(defn print-coords [coords]
  (doseq [y (range (:y *size*))
          x (range (:x *size*))
          :let [v (first (get-in coords [[x y] :possibilities]))]]
    (print (str v " "))
    (when (= x (dec (:x *size*)))
      (println))))

(defn create-result-map
  ([example-matrix] (create-result-map example-matrix (parse-size example-matrix)))
  ([example-matrix {:keys [x y]}]
   (binding [*size* {:x x :y y}
             *weights* (parse-weights example-matrix)
             *rules* (make-rules example-matrix)]
     (let [result (collapse-all (make-matrix-map example-matrix))]
       (print-coords result)))))

(defn generate-from-example-map [map]
  (binding [*rules* (make-rules map)
            *size* (parse-size map)
            *weights* (parse-weights map)]
   ; (make-matrix-map map)
    (create-result-map map {:x 10 :y 10})))

(def TEST-MAP
    [[:LAND_ :LAND_ :LAND_ :LAND_ :LAND_] 
    [:LAND_ :LAND_ :LAND_ :LAND_ :COAST] 
    [:LAND_ :LAND_ :LAND_ :COAST :SEA__] 
    [:LAND_ :COAST :COAST :COAST :SEA__] 
    [:COAST :SEA__ :SEA__ :SEA__ :SEA__] 
    [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__] 
     [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__]])

(def TEST-MAP2
    [[:LAND_ :LAND_ :LAND_ :LAND_ :LAND_]
     [:LAND_ :LAND_ :LAND_ :LAND_ :COAST]
     [:LAND_ :LAND_ :LAND_ :COAST :SEA__]
     [:LAND_ :COAST :COAST :COAST :SEA__]
     [:COAST :SEA__ :SEA__ :COAST :SEA__]
     [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__]
     [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__]])

(def TEST-MAP3
    [[:GRASS :FLOWR :GRASS :GRASS :GRASS]
     [:GRASS :GRASS :GRASS :GRASS :GRASS]
     [:GRASS :FLOWR :TREE_ :FLOWR :GRASS]
     [:GRASS :GRASS :TREE_ :BRNCH :GRASS]
     [:GRASS :BRNCH :TREE_ :GRASS :FLOWR]
     [:GRASS :GRASS :TREE_ :BRNCH :GRASS]
     [:GRASS :GRASS :TREE_ :GRASS :GRASS]])

(defn -main
  "Generates a new map based on the attributes of TEST-MAP above."
  [& args]
  (generate-from-example-map TEST-MAP2))





