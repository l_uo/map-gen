# map-gen

A library thing for generating maps using wave function collapse method. 

## Usage

Run the project directly:

    $ clojure -m luo.map-gen

This will just use one of the TEST-MAP variables defined in the map-gen.clj library file to
generate a similar map.

## Examples

Given an example map such as:

	"[[:LAND_ :LAND_ :LAND_ :LAND_ :LAND_]
     [:LAND_ :LAND_ :LAND_ :LAND_ :COAST]
     [:LAND_ :LAND_ :LAND_ :COAST :SEA__]
     [:LAND_ :COAST :COAST :COAST :SEA__]
     [:COAST :SEA__ :SEA__ :COAST :SEA__]
     [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__]
     [:SEA__ :SEA__ :SEA__ :SEA__ :SEA__]]"
	 
Will print out a new 10x10 map, something like this:

	":LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ 
	:LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ :LAND_ 
	:LAND_ :COAST :COAST :COAST :COAST :COAST :COAST :COAST :COAST :COAST 
	:LAND_ :COAST :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:LAND_ :COAST :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:COAST :COAST :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ 
	:SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__ :SEA__"	

